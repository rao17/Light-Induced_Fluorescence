# Light-Induced_Fluorescence
LIF Project

Code written in python to control Ocean Optics Spectrometer (QE65000) and 407nm/446nm diode lasers.

Uses ported Seabreeze API to control spectrometer and serial commands for the laser.

import seabreeze.spectrometers as sb
import numpy as np
import pyqtgraph as pg
import serial as sr
import sys
from pyqtgraph.Qt import QtGui, QtCore


# Initialize plots, spectrometer, and lasers
def initialize():

    global app1, app2, spec, curve1, curve2, laser407, laser446

    # Initialize plot
    app1 = QtGui.QApplication([])
    p1 = pg.plot()
    curve1 = p1.plot()

    app2 = QtGui.QApplication([])
    p2 = pg.plot()
    curve2 = p2.plot()

    # Initialize Spectrometer
    # sb.Spectrometer.close(self)
    devices = sb.list_devices()
    if devices == []:
        print("No devices found")
        sys.exit()
    spec = sb.Spectrometer(devices[0])
    spec.integration_time_micros(10000000)  # Set integration time to 100 milliseconds

    # Initialize Lasers
    laser407 = sr.Serial(port='COM7', baudrate=19200, bytesize=8, parity='N',
                         stopbits=1, timeout=1)
    laser446 = sr.Serial(port='COM8', baudrate=19200, bytesize=8, parity='N',
                         stopbits=1, timeout=1)
    laser407.write(b"L=0\r\n") # Start light off
    laser446.write(b"L=0\r\n") # Start light off


def update():
    wavelengths = np.array(spec.wavelengths())
    # NOTE BUG -> Obtaining spectrum doesnt restart the collection!!!!!
    # Check omnidriver manual
    # 407 nm laser
    # Possibly just use serial ttl
    laser407.write(b"L=1\r\n")
    intensitieslighton = np.array(spec.intensities())

    laser407.write(b"L=0\r\n")
    intensitieslightoff = np.array(spec.intensities())

    backgroundcorrected = intensitieslighton - intensitieslightoff

    curve1.setData(wavelengths, intensitieslighton)
    app1.processEvents()  # force complete redraw for every plot

    # 446 nm laser
    laser446.write(b"L=1\r\n")
    intensitieslighton = np.array(spec.intensities())

    laser446.write(b"L=0\r\n")
    intensitieslightoff = np.array(spec.intensities())

    backgroundcorrected = intensitieslighton - intensitieslightoff

    curve2.setData(wavelengths, backgroundcorrected)
    app2.processEvents()  # force complete redraw for every plot


def main():

    # Initialize plots, spectrometer, and lasers
    initialize()

    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(0)

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()


# Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    main()

